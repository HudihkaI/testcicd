//
//  TestCICDTests.swift
//  TestCICDTests
//
//  Created by Константин Ирошников on 28.10.2022.
//

import XCTest
@testable import TestCICD

class TestCICDTests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testTrue1() {
        let a = 4 * 10
        XCTAssertEqual(a, 40)
    }

    func testTrue2() {
        let a = 8 * 10
        XCTAssertEqual(a, 80)
    }

    func tesеTrue3() {
        let a = 8 * 10
        XCTAssertEqual(a, 40)
    }
}
